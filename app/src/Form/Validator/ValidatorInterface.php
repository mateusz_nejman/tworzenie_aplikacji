<?php

namespace App\Form\Validator;

interface ValidatorInterface
{
    public function validate($value): array;

    /**
     * @return bool
     */
    public function isFormValidator(): bool;
}