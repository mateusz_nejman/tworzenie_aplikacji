<?php


namespace App\Form\Validator;

use App\Services\UserManager;

class UniqueUsernameValidator implements ValidatorInterface
{
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param $value
     * @return array
     */
    public function validate($value): array
    {
        if (strlen($value) === 0) {
            return [];
        }

        $errors = [];
        if ($this->manager->isUserRegistered($value)) {
            $errors[] = sprintf('Użytkownik "%s" już istnieje.', $value);
        }

        return $errors;
    }

    /**
     * @return bool
     */
    public function isFormValidator(): bool
    {
        return false;
    }
}