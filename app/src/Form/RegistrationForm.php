<?php

namespace App\Form;

use App\Form\Validator\EqualValidator;
use App\Form\Validator\NotNullValidator;
use App\Form\Validator\UniqueUsernameValidator;
use App\Form\Widget\InputWidget;
use App\Form\Widget\SubmitWidget;
use App\Services\UserManager;

class RegistrationForm extends Form
{
    public function __construct(string $name, string $action, UserManager $manager)
    {
        parent::__construct($name, $action);

        $this->addField('username', new Field(
            new InputWidget(),
            [
                new NotNullValidator(),
                new UniqueUsernameValidator($manager)
            ],
            [
                'label' => 'Nazwa użytkownika'
            ]
        ));

        $this->addField('password', new Field(
            new InputWidget(InputWidget::TYPE_PASSWORD),
            [
                new NotNullValidator()
            ],
            [
                'label' => 'Hasło'
            ]
        ));

        $this->addField('repeatPassword', new Field(
            new InputWidget(InputWidget::TYPE_PASSWORD),
            [
                new NotNullValidator()
            ],
            [
                'label' => 'Powtórz hasło'
            ]
        ));

        $this->addField('submit', new Field(
            new SubmitWidget('Zarejestruj')
        ));

        $this->addValidator(new EqualValidator(['password', 'repeatPassword'], 'repeatPassword'));
    }
}