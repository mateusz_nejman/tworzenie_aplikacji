<?php

namespace App\Form;

use App\Form\Validator\ValidatorInterface;
use App\Form\Widget\WidgetInterface;
use App\Request;

class Form
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var Field[]
     */
    protected $fields;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var bool
     */
    private $submitted = false;

    /**
     * @var bool
     */
    private $valid = false;

    protected $errors = [];

    /**
     * @var ValidatorInterface[]
     */
    private $validators = [];

    /**
     * @param string $name
     * @param string $action
     * @param string $method
     * @param Field[] $fields
     */
    public function __construct(string $name, string $action, string $method = 'POST', array $fields = [])
    {
        $this->name = $name;
        $this->method = $method;
        $this->action = $action;
        $this->fields = $fields;
    }

    /**
     * @param ValidatorInterface $validator
     */
    public function addValidator(ValidatorInterface $validator)
    {
        $this->validators[] = $validator;
    }

    /**
     * @param string $name
     * @param WidgetInterface $widget
     * @param ValidatorInterface[] $validators
     */
    public function addWidget(string $name, WidgetInterface $widget, array $validators = [], array $options = [])
    {
        $this->fields[$name] = new Field($widget, $validators, $options);
    }

    /**
     * @param string $name
     * @param Field $field
     */
    public function addField(string $name, Field $field)
    {
        $this->fields[$name] = $field;
    }

    public function render()
    {
        $html = <<<HTML
<form class="form" name="{$this->name}" method="{$this->method}" action="{$this->action}">
    %s
</form>
HTML;

        return sprintf($html, $this->renderFields());
    }

    public function handleRequest(Request $request)
    {
        if (!$request->isMethod($this->method)) {
            return;
        }

        $this->submitted = true;
        if (strtoupper($this->method) === 'GET') {
            $this->data = $request->getQueryParam($this->name, []);
        } else {
            $this->data = $request->getPost($this->name, []);
        }
        $this->cleanValues();
        $this->validate();
    }

    public function isSubmitted(): bool
    {
        return $this->submitted;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $fieldName
     * @return string
     */
    protected function generateName(string $fieldName)
    {
        return sprintf('%s[%s]', $this->name, $fieldName);
    }

    /**
     * @param string $fieldName
     * @return string
     */
    protected function generateId(string $fieldName)
    {
        return sprintf('%s_%s', $this->name, $fieldName);
    }

    protected function renderLabel(string $name)
    {
        $field = $this->fields[$name];
        $label = $field->getOption('label', false);
        if (!$label) {
            return '';
        }

        $id = $this->generateId($name);
        $label = $field->getOption('label');

        return sprintf('<label for="%s">%s</label>', $id, $label);
    }

    protected function cleanValues()
    {
        $cleanedData = [];
        foreach ($this->data as $name => $value) {
            $value = trim($value);

            $cleanedData[$name] = $value !== '' ? $value : null;
        }
        $this->data = $cleanedData;
    }

    private function renderFields()
    {
        $fieldTemplate = <<<HTML
<div class="form__field">
    __label__
    __errors__
    __widget__
</div>
HTML;

        $html = [];
        foreach ($this->fields as $name => $field) {
            $value = $this->data[$name] ?? null;
            $fullName = $this->generateName($name);
            $id = $this->generateId($name);

            $fieldHtml = str_replace('__widget__', $field->renderWidget($fullName, $id, $value, $field->getOptions()), $fieldTemplate);
            $fieldHtml = str_replace('__label__', $this->renderLabel($name), $fieldHtml);
            $fieldHtml = str_replace('__errors__', $this->renderErrors($name), $fieldHtml);
            $html[] = $fieldHtml;
        }

        return implode('', $html);
    }

    private function renderErrors($fieldName)
    {
        if (!($this->errors[$fieldName] ?? [])) {
            return '';
        }

        $errorRowsHtml = [];
        foreach ($this->errors[$fieldName] as $errorMessage) {
            $errorRowsHtml[] = sprintf('<li>%s</li>', $errorMessage);
        }

        return sprintf('<ul class="form__errors">%s</ul>', implode('', $errorRowsHtml));
    }

    private function validate()
    {
        foreach ($this->fields as $name => $field) {
            $errors = $field->validate($this->data[$name] ?? null);
            if (!empty($errors)) {
                $this->errors[$name] = $errors;
            }
        }

        foreach ($this->validators as $validator) {
            if ($validator->isFormValidator()) {
                foreach ($validator->validate($this->data) as $fieldName => $errors) {
                    foreach ($errors as $message) {
                        $this->errors[$fieldName] = $message;
                    }
                }
            }
        }

        $this->valid = empty($this->errors);
    }
}