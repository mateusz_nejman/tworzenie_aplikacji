<?php

namespace App\Form\Widget;

class SubmitWidget implements WidgetInterface
{
    private $label;

    private $primary;

    public function __construct(string $label, bool $primary = true)
    {
        $this->label = $label;
        $this->primary = $primary;
    }

    public function render(string $name, string $id, $value, array $fieldOptions = []): string
    {
        $cssClass = $this->primary ? 'button-primary' : '';
        $html = <<<HTML
<button class="button {$cssClass}" type="submit" name="{$name}" id="{$id}">{$this->label}</button>
HTML;

        return $html;
    }
}