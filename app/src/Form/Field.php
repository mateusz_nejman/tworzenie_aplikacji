<?php

namespace App\Form;

use App\Form\Validator\ValidatorInterface;
use App\Form\Widget\WidgetInterface;

class Field
{
    /**
     * @var WidgetInterface
     */
    private $widget;
    /**
     * @var ValidatorInterface[]
     */
    private $validators;
    /**
     * @var array
     */
    private $options;

    /**
     * Field constructor.
     * @param WidgetInterface $widget
     * @param ValidatorInterface[] $validators
     * @param array $options
     */
    public function __construct(
        WidgetInterface $widget,
        array $validators = [],
        array $options = []
    )
    {
        $this->widget = $widget;
        $this->validators = $validators;
        $this->options = $options;
    }

    /**
     * @param string $name
     * @param string $id
     * @param $value
     * @return string
     */
    public function renderWidget(string $name, string $id, $value, array $fieldOptions)
    {
        return $this->widget->render($name, $id, $value, $fieldOptions);
    }

    /**
     * @return array
     */
    public function validate($value)
    {
        $errors = [];
        foreach ($this->validators as $validator) {
            foreach ($validator->validate($value) as $message) {
                $errors[] = $message;
            }
        }

        return $errors;
    }

    public function getOption(string $name, $default = null)
    {
        return $this->options[$name] ?? $default;
    }

    public function getOptions()
    {
        return $this->options;
    }
}