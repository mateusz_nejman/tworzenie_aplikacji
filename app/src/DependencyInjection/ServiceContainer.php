<?php

namespace App\DependencyInjection;

use App\Controllers\LoginCheckController;
use App\Controllers\LogoutController;
use App\Controllers\PageController;
use App\Controllers\RegistrationController;
use App\Layout;
use App\Repository\ChainUserRepository;
use App\Repository\CsvFileUserRepository;
use App\Repository\InMemoryUserRepository;
use App\Repository\JsonFileUserRepository;
use App\Repository\PdoUserRepository;
use App\Router;
use App\Security\Sha1PasswordEncoder;
use App\Services\UserManager;
use App\Session\Session;

class ServiceContainer
{
    private static $instance;

    /**
     * @var array
     */
    private $factories = [];

    /**
     * @var array
     */
    private $services = [];

    /**
     * @var Injector[]
     */
    private $injectors = [];

    public function __construct()
    {
        $this->factories['router'] = function (ServiceContainer $container) {
            $router = new Router();

            $router->addRoute('homepage', [
                'path' => '/',
                'controller' => function () use ($container) {
                    return $container->get('controller.homepage');
                }
            ]);

            $router->addRoute('article', [
                    'path' => '/article/{id}',
                    'controller' => function () use ($container) {
                        return $container->get('controller.article');
                    }
            ]);

            $router->addRoute('login_check', [
                'path' => '/login-check',
                'controller' => function () use ($container) {
                    return $container->get('controller.login_check');
                }
            ]);

            $router->addRoute('logout', [
                'path' => '/logout',
                'controller' => function () use ($container) {
                    return $container->get('controller.logout');
                }
            ]);

            $router->addRoute('registration', [
                'path' => '/registration',
                'controller' => function () use ($container) {
                    return $container->get('controller.registration');
                }
            ]);

            return $router;
        };

        $this->factories['session'] = function () {
            return new Session();
        };

        $this->factories['password_encoder'] = function () {
            return new Sha1PasswordEncoder();
        };

        $this->factories['user_repository'] = function ($container) {
            $repository = new ChainUserRepository();
            $repository->addRepository('pdo', new PdoUserRepository('sqlite:///' . __DIR__ . '/../../data/users.db'));
            $repository->addRepository('json', new JsonFileUserRepository(__DIR__ . '/../../data/users.json'));
            $repository->addRepository('csv', new CsvFileUserRepository(__DIR__ . '/../../data/users.csv'));
            $repository->addRepository('in_memory', InMemoryUserRepository::createFromPlainPasswords($container->get('password_encoder'), [
                'test5' => 'test123',
                'test6' => 'test123'
            ]));

            $repository->setSaveRepository('json');

            return $repository;
        };

        $this->factories['user_manager'] = function (ServiceContainer $container) {
            return new UserManager(
                $container->get('user_repository'),
                $container->get('password_encoder')
            );
        };

        $this->factories['templating'] = function (ServiceContainer $container) {
            return new Layout($container->get('session'));
        };

        $this->factories['controller.registration'] = function (ServiceContainer $container) {
            return new RegistrationController(
                $container->get('user_manager')
            );
        };

        $this->factories['controller.login_check'] = function (ServiceContainer $container) {
            return new LoginCheckController(
                $container->get('user_repository'),
                $container->get('password_encoder')
            );
        };

        $this->factories['controller.logout'] = function () {
            return new LogoutController();
        };

        $this->factories['controller.article'] = function () {
            return new PageController( 'article');
        };

        $this->factories['controller.homepage'] = function () {
            return new PageController('homepage');
        };
    }

    /**
     * @param Injector $injector
     */
    public function registerInjector(Injector $injector)
    {
        $this->injectors[] = $injector;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function get($id)
    {
        if (!$this->has($id)) {
            throw new \Exception(sprintf('Service "%s" not defined', $id));
        }

        if (!isset($this->services[$id])) {
            $service = $this->factories[$id]($this);

            foreach ($this->injectors as $injector) {
                $injector->inject($this, $service);
            }

            $this->services[$id] = $service;
        }

        return $this->services[$id];
    }

    /**
     * @param $id
     * @return bool
     */
    public function has($id)
    {
        return isset($this->factories[$id]);
    }
}