<?php

namespace App\Controllers;

use App\Request;
use App\Response\RedirectResponse;
use App\Response\Response;

class LogoutController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request): Response
    {
        $this->session->destroy();

        return new RedirectResponse(
            $this->router->generate('homepage')
        );
    }
}