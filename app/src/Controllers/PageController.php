<?php

namespace App\Controllers;

use App\Controllers\Traits\TemplatingAwareTrait;
use App\DependencyInjection\TemplatingAwareInterface;
use App\Request;
use App\Response\Response;
use App\Router;

class PageController extends AbstractController implements TemplatingAwareInterface
{
    use TemplatingAwareTrait;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $layout;

    /**
     * PageController constructor.
     * @param Router $router
     * @param string $name
     * @param string $layout
     */
    public function __construct(string $name, string $layout = 'default')
    {
        $this->name = $name;
        $this->layout = $layout;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        return new Response(
            $this->templating->render($this->name, [
                'request' => $request,
                'router' => $this->router
            ], $this->layout)
        );
    }
}