<?php

namespace App\Controllers;

use App\Controllers\Traits\BaseControllerTrait;
use App\DependencyInjection\BaseControllerInterface;

abstract class AbstractController implements ControllerInterface, BaseControllerInterface
{
    use BaseControllerTrait;
}