<?php

namespace App\Controllers;

use App\Controllers\Traits\TemplatingAwareTrait;
use App\DependencyInjection\TemplatingAwareInterface;
use App\Form\RegistrationForm;
use App\Request;
use App\Response\RedirectResponse;
use App\Response\Response;
use App\Router;
use App\Services\UserManager;

class RegistrationController extends AbstractController implements TemplatingAwareInterface
{
    use TemplatingAwareTrait;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @param Router $router
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request): Response
    {
        $form = new RegistrationForm(
            'registration',
            $this->router->generate($request->getAttribute('routeName'), $request->getPathParameters()),
            $this->userManager
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $this->userManager->registerUser($formData['username'], $formData['password']);
            $this->session->setFlashMessage('success', 'Rejestracja przebiegła pomyślnie. Teraz możesz się zalogować podając dane, które zostały wprowadzone w formularzu rejestracji.');

            return new RedirectResponse($this->router->generate('homepage'));
        }

        return new Response($this->templating->render('registration', [
            'request' => $request,
            'router' => $this->router,
            'form' => $form
        ]));
    }
}