<?php

namespace App\Controllers\Traits;

use App\Router;
use App\Session\Session;

trait BaseControllerTrait
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param Router $router
     */
    public function injectRouter(Router $router): void
    {
        $this->router = $router;
    }

    /**
     * @param Session $session
     */
    public function injectSession(Session $session): void
    {
        $this->session = $session;
    }
}