<?php


namespace App\Repository;


use App\Model\UserCredentials;

class PdoUserRepository implements UserRepositoryInterface
{
    private $pdo;

    public function __construct(string $dsn, string $username = null, string $password = null)
    {
        $this->pdo = new \PDO($dsn, $username, $password);
    }

    /**
     * @param string $username
     * @return UserCredentials|null
     */
    public function findCredentialsByUsername(string $username): ?UserCredentials
    {
        $sql = "SELECT * FROM users WHERE username = :username";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':username', $username, \PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$row) {
            return null;
        }

        return new UserCredentials($row['username'], $row['password']);
    }

    /**
     * @param UserCredentials $credentials
     */
    public function saveUser(UserCredentials $credentials): void
    {
        // TODO: implement
    }
}