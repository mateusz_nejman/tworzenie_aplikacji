<?php

namespace App\Exception\User;

class UserAlreadyExistsException extends \Exception
{
    /**
     * @param string $username
     * @return UserAlreadyExistsException
     */
    public static function withUsername(string $username): self
    {
        return new self(sprintf('User with username "%s" already exists.', $username));
    }
}