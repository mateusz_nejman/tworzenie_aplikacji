<html>
    <head>
        <title><?php echo $title ?></title>
        <link rel="stylesheet" href="/styles/app.css"
    </head>
    <body>
        <nav class="menu">
            <ul>
                <li class="menu-item"><a href="<?php echo $router->generate('homepage') ?>">Homepage</a></li>
                <li class="menu-item"><a href="<?php echo $router->generate('registration'); ?>">Rejestracja</a></li>
                <li class="menu-item"><a href="<?php echo $router->generate('article', ['id' => 1]) ?>">Article1</a></li>
                <li class="menu-item menu-item-separator"></li>
                <li class="menu-item menu-item-login-box">
                    <?php include 'parts/loginForm.php' ?>
                </li>
            </ul>
        </nav>
        <div class="content">
            <h1>Hello world</h1>
            <?php include 'parts/flashMessages.php' ?>


            <?php echo $content; ?>
        </div>
    </body>
</html>